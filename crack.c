#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be

struct entry 
{
    char plain[50];
    char hash[33];
};

int comp(const void *a, const void *b)
{
	struct entry *aa = (struct entry *)a;
	struct entry *bb = (struct entry *)b;
	
	return strcmp(aa->hash, bb->hash);
}
int lookuphash(const void *t, const void *elem)
{
	char *tt = (char *)t;
	struct entry *eelem = (struct entry *)elem;
	
	return strcmp(tt, eelem->hash);
}
void printFound(struct entry find);

struct entry *read_dictionary(char *filename, int *size)
{
    FILE *f = fopen(filename, "r");
    if (f == NULL)
    {
        perror("Can't open given file");
        exit(1);
    }
    int arraylength = 10;
    struct entry *in = malloc(arraylength * sizeof(struct entry));
	int entries = 0;
	char line[20];
	while(fgets(line, 20, f) != NULL)
	{
		char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
		if(entries == arraylength)
		{
			arraylength += 10;
			in = realloc(in, arraylength*sizeof(struct entry));
		}
		strcpy(in[entries].plain, line);
		char *hash = md5(line, strlen(line));
		strcpy(in[entries].hash, hash);
		entries++;
	}
    fclose(f);
    *size = entries;
    return in;
}


int main(int argc, char *argv[])
{
    int size;
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    struct entry *dict = read_dictionary(argv[2], &size);
    qsort(dict, size, sizeof(struct entry), comp);
    FILE *hf = fopen(argv[1], "r");
    if (hf == NULL)
    {
        perror("Can't open given file");
        exit(1);
    }
    char line[34];
	while(fgets(line, 34, hf) != NULL)
	{
		char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        struct entry *found = bsearch(line, dict, size, sizeof(struct entry), lookuphash);
        if(found)
        {
            printFound(*found);
        }
	}
        return 0;
}
void printFound(struct entry find)
{
	printf("Plain:%s Hash:%s\n", find.plain, find.hash);
}
